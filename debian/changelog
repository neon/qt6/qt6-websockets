qt6-websockets (6.8.2-0neon) noble; urgency=medium

  * New release

 -- Carlos De Maine <carlosdemaine@gmail.com>  Sun, 09 Feb 2025 09:53:26 +1000

qt6-websockets (6.8.1-0neon) noble; urgency=medium

  * New release

 -- Carlos De Maine <carlosdemaine@gmail.com>  Sun, 08 Dec 2024 08:02:11 +1000

qt6-websockets (6.8.0-0neon) noble; urgency=medium

  * New release

 -- Carlos De Maine <carlosdemaine@gmail.com>  Wed, 24 Jul 2024 21:42:52 +1000

qt6-websockets (6.7.0-0neon) jammy; urgency=medium

  * New release

 -- Carlos De Maine <carlosdemaine@gmail.com>  Sat, 17 Feb 2024 09:54:27 +1000

qt6-websockets (6.6.1-0neon) jammy; urgency=medium

  [ Patrick Franz ]
  * New release

  [ Carlos De Maine ]
  * New release

 -- Carlos De Maine <carlosdemaine@gmail.com>  Sat, 09 Dec 2023 13:04:18 +1000

qt6-websockets (6.4.2-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (6.4.2).
  * Bump Qt B-Ds to 6.4.2.
  * Update ABI to 6.4.2.

 -- Patrick Franz <deltaone@debian.org>  Wed, 16 Nov 2022 06:59:48 +0100

qt6-websockets (6.4.0-2) experimental; urgency=medium

  [ Patrick Franz ]
  * Rename libqt6websockets6-dev to qt6-websockets-dev.
  * New binary package qt6-websockets-private-dev.
  * Update d/copyright.

 -- Patrick Franz <deltaone@debian.org>  Sun, 06 Nov 2022 13:25:45 +0100

qt6-websockets (6.4.0-1) experimental; urgency=medium

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * Bump minimum CMake version in order to get pkg-config's .pc files.

  [ Patrick Franz ]
  * Increase CMake verbosity level.
  * New upstream release (6.4.0).
  * Bump Qt B-Ds to 6.4.0.
  * Update list of installed files.
  * Update symbols from buildlogs.

 -- Patrick Franz <deltaone@debian.org>  Thu, 13 Oct 2022 12:37:43 +0200

qt6-websockets (6.3.1-2) unstable; urgency=medium

  [ Patrick Franz ]
  * Upload to unstable.

 -- Patrick Franz <deltaone@debian.org>  Mon, 15 Aug 2022 19:24:16 +0200

qt6-websockets (6.3.1-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (6.3.1).
  * Bump Qt B-Ds to 6.3.1.
  * Bump Standards-Version to 4.6.1 (no changes needed).
  * Update list of installed files.
  * Update symbols from buildlogs.

 -- Patrick Franz <deltaone@debian.org>  Sun, 17 Jul 2022 03:43:52 +0200

qt6-websockets (6.3.0-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (6.3.0).
  * Bump Qt B-Ds to 6.3.0.
  * Update list of installed files.

 -- Patrick Franz <deltaone@debian.org>  Tue, 14 Jun 2022 21:01:13 +0200

qt6-websockets (6.2.4-2) unstable; urgency=medium

  [ Patrick Franz ]
  * Upload to unstable.

 -- Patrick Franz <deltaone@debian.org>  Fri, 22 Apr 2022 20:08:04 +0200

qt6-websockets (6.2.4-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (6.2.4).
  * Bump Qt B-Ds to 6.2.4.

 -- Patrick Franz <deltaone@debian.org>  Wed, 30 Mar 2022 21:14:07 +0200

qt6-websockets (6.2.2-3) unstable; urgency=medium

  [ Patrick Franz ]
  * Upload to unstable.

 -- Patrick Franz <deltaone@debian.org>  Tue, 15 Feb 2022 22:04:38 +0100

qt6-websockets (6.2.2-2) experimental; urgency=medium

  [ Patrick Franz ]
  * Update symbols from buildlogs.

 -- Patrick Franz <deltaone@debian.org>  Mon, 06 Dec 2021 02:17:32 +0100

qt6-websockets (6.2.2-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (6.2.2).
  * Bump Qt B-Ds to 6.2.2.
  * Update installed files.

 -- Patrick Franz <deltaone@debian.org>  Mon, 06 Dec 2021 00:48:34 +0100

qt6-websockets (6.2.1-1) experimental; urgency=medium

  * Initial release (Closes: #1000167)

 -- Patrick Franz <deltaone@debian.org>  Wed, 24 Nov 2021 13:40:44 +0100
